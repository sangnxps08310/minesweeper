/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toy.impl;

import com.toy.factory.MainModelFactory;

/**
 *
 * @author
 * nhm95
 */
public class MainImpl {
  
  public static MainModelFactory mainModelFactory;

  static {
    mainModelFactory = new MainModelFactory();
  }
  public static MainModelFactory getInstance() {
    return mainModelFactory;
  }
}
