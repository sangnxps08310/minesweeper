package com.toy.impl;

import com.toy.factory.PanelFactory;
import com.toy.lib.Panel;
import javax.swing.JFrame;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author
 * nhm95
 */
public class MainFrameImpl extends javax.swing.JFrame {

  public final Panel container = PanelFactory.get(PanelFactory.CONTAINER);
  public final Panel header = PanelFactory.get(PanelFactory.HEADER);
  public final Panel body = PanelFactory.get(PanelFactory.BODY);

  public void initDefaultSetting() {
    this.setSize(460, 720);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    header.init();
    body.init();
    container.init();
    this.add(header);
    this.add(body);
    body.add(container);
  }

  public void message() {
    System.out.println(this.getTitle());
  }
}
