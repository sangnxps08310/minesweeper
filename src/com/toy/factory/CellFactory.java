/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toy.factory;

import com.toy.lib.Cell;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author
 * nhm95
 */
public class CellFactory {

  private static Map<String, Cell> cellMap = new HashMap<String, Cell>();

  public static Cell getCell(String id) {
    return cellMap.get(id);
  }
  
}
