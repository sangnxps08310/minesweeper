/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toy.factory;
import com.toy.impl.MainFrameImpl;
import com.toy.ui.MainFrame;

/**
 *
 * @author
 * nhm95
 */
public class MainFrameFactory {

  public static MainFrameImpl mainFrame;

  static {
    mainFrame = new MainFrame();
    mainFrame.initDefaultSetting();
  }

  public static MainFrameImpl getInstance() {
    return mainFrame;
  }
}
