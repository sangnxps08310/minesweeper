/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toy.factory;

import com.toy.lib.Panel;
import com.toy.ui.Body;
import com.toy.ui.Container;
import com.toy.ui.Header;
import java.awt.Color;
import javax.swing.JPanel;

/**
 *
 * @author
 * nhm95
 */
public class PanelFactory extends Panel {

  public static final String BODY = "body";
  public static final String HEADER = "header";
  public static final String CONTAINER = "container";

  private static PanelFactory body;
  private static PanelFactory header;
  private static PanelFactory container;

  static {
    body = new Body();
    header = new Header();
    container = new Container();
  }

  @Override
  public void init() {
    switch (this.getName()) {
      case BODY:
        initBody();
        break;
      case HEADER:
        initHeader();
        break;
      case CONTAINER:
        initContainer();
        break;
    }
  }

  private void initBody() {
    this.setSize(460, 680);
//    this.setBackground(Color.BLUE);
//    this.add(container);
    this.repaint();
    System.out.println("initial body");

  }

  private void initContainer() {
    this.setSize(460, 680);
    this.setBackground(Color.red);
    System.out.println("initial container");
  }

  private void initHeader() {
    this.setSize(460, 60);
    this.setBackground(Color.yellow);
    System.out.println("initial header");

  }

  public static Panel get(String panel) {
    switch (panel) {
      case BODY:
        return body;
      case HEADER:
        return header;
      case CONTAINER:
        return container;
    }
    return null;
  }
}
