/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toy.model;

import com.toy.factory.MainFrameFactory;

/**
 *
 * @author
 * nhm95
 */
public class MainRunnable implements Runnable {

  public MainRunnable() {
  }

  @Override
  public void run() {
    try {
      MainFrameFactory.getInstance().setVisible(true);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
