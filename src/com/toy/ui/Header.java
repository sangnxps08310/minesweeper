/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toy.ui;

import com.toy.factory.PanelFactory;
import java.awt.Color;

/**
 *
 * @author
 * nhm95
 */
public class Header extends PanelFactory {

  /**
   * Creates
   * new
   * form
   * Header
   */
  public Header() {
    initComponents();
    this.setName("header");
  }

  /**
   * This
   * method
   * is
   * called
   * from
   * within
   * the
   * constructor
   * to
   * initialize
   * the
   * form.
   * WARNING:
   * Do
   * NOT
   * modify
   * this
   * code.
   * The
   * content
   * of
   * this
   * method
   * is
   * always
   * regenerated
   * by
   * the
   * Form
   * Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    lblMine = new javax.swing.JLabel();
    lblScore = new javax.swing.JLabel();
    btnReset = new javax.swing.JButton();

    setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
    add(lblMine, new org.netbeans.lib.awtextra.AbsoluteConstraints(343, 12, 105, 36));
    add(lblScore, new org.netbeans.lib.awtextra.AbsoluteConstraints(22, 12, 105, 36));
    add(btnReset, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 10, 38, 36));
  }// </editor-fold>//GEN-END:initComponents


  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JButton btnReset;
  private javax.swing.JLabel lblMine;
  private javax.swing.JLabel lblScore;
  // End of variables declaration//GEN-END:variables
}
