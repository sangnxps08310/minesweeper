/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toy.lib;

import java.awt.event.MouseEvent;

/**
 *
 * @author
 * nhm95
 */
public interface CellAction {

  public abstract void rightClick(MouseEvent e);

  public abstract void leftClick(MouseEvent e);

  public abstract void end();

  public abstract void Score();
  

}
