/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.toy.lib;

import com.sun.org.apache.xml.internal.security.Init;
import com.toy.ui.CellContainer;
import java.awt.event.MouseEvent;

/**
 *
 * @author
 * nhm95
 */
public abstract class Cell extends CellContainer implements Cloneable {

  private String id;
  protected String status;
  protected String type;

  public Cell() {
  }

  public Cell(String id, String status, String type) {
    this.id = id;
    this.status = status;
    this.type = type;
    init();
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Object clone() {
    Object clone = null;
    try {
      clone = super.clone();
    } catch (CloneNotSupportedException e) {
      e.printStackTrace();
    }
    return clone;
  }

  public void init() {
    
  }
}
